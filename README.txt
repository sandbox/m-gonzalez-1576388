README.txt
==========

This module provides a set of tools for securing your site.

Currently done:

  -Disables the attribute "AUTOCOMPLETE" the login form for username and password, and any field "password" for any form of your site.
  -XSS-cleaner body and title of any node.
  -Validates text fields of the nodes, avoiding persistent XSS in database.
  -It provides functions that can be used by other developers to validate form fields. 
    It does not validate the fields of a node, other forms only
  -It provides functions that can be used by other developers to validate a valid AJAX requests. 
    This is a simple function, that can be used in an ajax callback function to determinate 
    if is an ajax process or is a normal page request directly in the browser
  -Avoid direct access to paths of type node/ID if the node has a URL alias (pathauto).


AUTHOR/MAINTAINER
======================
-Mart�n Gonz�lez Robles