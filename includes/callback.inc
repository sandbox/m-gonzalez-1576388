<?php
/**
 * @file 
 *   callback.inc
 * @author Martin Gonzalez
 */

/**
 * Validate access to certain callbacks
 * @param $destination
 * @see request_uri()
 */
function _secure_access_callback($destination) {  
  $check_callback_strings = array(
      '0' => '/node\/\d/'  //directly to node
  );
 
  for ($iter = 0; $iter < count($check_callback_strings); $iter++) {
    if (preg_match($check_callback_strings[$iter], $destination)) {
      switch ($iter) {
      
        case 0:
          
          redirect_to_alias($destination);
          
        break;
        
        default:
          ;
        break;
      }
     
    }
  }
}



/**
 * Managed when a user is directly accessing the path of a node (node / ID) 
 * and not to the URL alias, redirecting the user, to the alias of the 
 * node path if you have permission
 * @param $destination
 */
function redirect_to_alias($destination) {
  if (module_exists("pathauto")) {
    module_load_include('inc', "securitytools", 'includes/securitytools');
    global $user;
    //if is root, $user->uid = 1
    if ( is_root($user) ) {
      return;
    }
    
    $destination = str_replace_first(base_path(), '', $destination);
    
    $path = explode('/', $destination);
    $node = node_load($path[1]);
    if ( ! $node) {//create
      return;
    }
    elseif ($path[2] === 'edit') {
      return;
    }
    elseif ($path[2] === 'track') {
      return;
    }
    elseif ($path[2] === 'workflow') {
      return;
    }
    //need to add more cases to allow access to routes that need the type node/ID/SOMETHING
    
    $path = drupal_get_path_alias($destination);
    drupal_goto($path);
  }
}

/**
 * Replaces the first element of a string
 * @param $search
 * @param $replace
 * @param $subject
 */
function str_replace_first($search, $replace, $subject) {
  $pos = strpos($subject, $search);
  if ($pos !== FALSE) {
    $subject = substr_replace($subject, $replace, $pos, strlen($search));
  }
  return $subject;
}