<?php
/**
 * @file
 * securitytools.inc
 * @author Mart�n Gonz�lez Robles
 */

/**
 *
 * Rate if you have a specific role
 * @param $user
 * @param string $rolname
 * @return boolean
 *   TRUE if you have the role, FALSE in other case
 */
function have_this_role($user, $rolname) {

  if ( ! in_array($rolname, $user->roles)) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Rate if the root system
 * @param $user
 * @return boolean
 */
function is_root($user) {

  if ($user->uid <> 1) {
    return FALSE;
  }

  return TRUE;

}

/**
 * Checks if the user, is the author of the node
 * @param $node
 * @param $uid
 * @return boolean
 * 	TRUE if is the author, FALSE in other case
 */
function is_autor($node, $uid) {

  if ($node->uid <> $uid) {
    return FALSE;
  }

  return TRUE;
}