<?php
/**
 * @file validate.inc
 * @author Martin Gonzalez
 */
define('XSS_INTENT', t('Invalid format text'));


/**
 * Form Field validate
 * It does not validate the fields of a node, other forms only
 */
function securitytools_field_validate($field, $form_values) {
  
  //getting allowed html tags
  $allowed_html = get_allowed_html();

  $field_name = $field['#title'];
  $field_value = $field['#value'];

  validate_node_field($field_name, $field_value, $allowed_html, $field);

}


/**
 * Option view del node api
 *
 * "view":
 *   The node content is being assembled before rendering. 
 *   The module may add elements $node->content prior to rendering. 
 *   This hook will be called after hook_view(). 
 *   The format of $node->content is the same as used by Forms API.
 * @param $node
 */
function validate_node_view($node) {
  //getting allowed html tags
  $allowed_html = get_allowed_html();
  
  //secure node title
  if ($node->title) {
    $node->title = filter_xss($node->title, $allowed_html);
  }

  
}//end function


/**
 * Option validate del node api
 * 
 * "validate": 
 *   The user has just finished editing the node and is trying to preview
 *   or submit it. This hook can be used to check the node data. 
 *   Errors should be set with form_set_error().
 * @param $node
 */
function validate_node_save($node) {

  $validate = TRUE;//BY DEFAULT TRUE
  
  //getting allowed html tags 
  $allowed_html = get_allowed_html();
  
  if ( ! validate_node($node, $allowed_html) ) {
    return;
  }

  //get the node as an array to access their fields with variables
  $node_fields = get_object_vars($node);

  //I get the content type fields
  $fields = _get_node_name_fields($node->type);
  foreach ($fields as $field) {
    
    if ($field['widget_module'] === 'text') {
      
      //validate each field
      
      $field_name = trim($field['field_name']); 
      
      $value = $node_fields[$field_name][0]['value'];

      if (isset($value)) {
        
        if ( ! validate_node_field($field['label'], $value, $allowed_html)) {//if the field is not validated
          return;
        }
        
      }
      
    }//end if ===text
    
  }//end foreach
  
}//end function 


/**
 * Gets the list of content type fields
 * @param string $type
 *   content type
 * @return array $fields
 *   array list of content type fields
 */
function _get_node_name_fields($type) {
  $fields = array();
  $query = "SELECT field_name, label, widget_module FROM {content_node_field_instance} c WHERE type_name = '%s'";
  $result = db_query($query, $type);
  $iter = 0;
  while ($data = db_fetch_array($result)) {
    $fields[$iter]['field_name'] = $data['field_name'];
    $fields[$iter]['label'] = $data['label'];
    $fields[$iter]['widget_module'] = $data['widget_module'];
    
    $iter++;
  }
  return $fields;
}

/**
 * Validate a field
 * @param string $field
 *   field label
 * @param string $value
 *   field value
 * @param array $allowed_html
 *   allowed html tags
 * @param array $element
 *   element to validate
 */
function validate_node_field($field, $value, $allowed_html, $element = NULL) {

  if ($value !=  filter_xss($value, $allowed_html) ) {
    if ($element) {
      form_error($element, $field . ': ' . XSS_INTENT);
    }
    else {
      form_set_error('error', $field . ': ' . XSS_INTENT);
    }
    return FALSE;
  }
  
  return TRUE;
}


/**
 * Validates the title and body
 * @param $node
 * @param array $allowed_html
 * @return boolean
 */
function validate_node($node, $allowed_html) {
  $query = "SELECT has_body, has_title FROM {node_type} n WHERE type = '%s'";
  $values = db_fetch_array(db_query($query, $node->type));
  if ($values['has_title'] == 1) {
    if ( ! validate_node_title($node, $allowed_html) ) return FALSE;
  }
  if ($values['has_body'] == 1) {
    if ( ! validate_node_body($node) ) return FALSE;
  }
  
  return TRUE;
  
}

/**
 * validate title
 * @param $node
 * @param array $allowed_html
 */
function validate_node_title($node, $allowed_html) {
  
  return validate_node_field(t('title'), $node->title, $allowed_html);
  
}
 
/**
 * Validates the body of the node to have no PHP or Javascript code
 * You can modify this function to be a more restrictive validation.
 * @param $node
 * @return boolean
 */
function validate_node_body($node) {
  $sub_str_php = '<?php';
  $sub_str_xss = '<script';
  $cadena = ' ' . $node->body . '';
  if ( strpos($cadena , $sub_str_php) ||  strpos($cadena , $sub_str_xss)) {
    form_set_error('error', t('Body: The format is not correct'));
    return FALSE;
  }
    
  return TRUE;
}

/**
 * get an array with allowed html tags
 * @return array $allowed_html
 */
function get_allowed_html() {

  $allowed_html = variable_get('allowed_html_1', '');
  
  $allowed_html = explode(' ', $allowed_html);
  
  for ($iter = 0; $iter < count($allowed_html); $iter++) {
    $allowed_html[$iter] = str_replace('<', '', $allowed_html[$iter]);
    $allowed_html[$iter] = str_replace('>', '', $allowed_html[$iter]);
  }
  
  return $allowed_html;
}


/**
 * secure a string. Returns the string after passing the filter with tags allowed
 * @param $string
 * @param string
 *   filter string
 */
function securing_field($string) {

  //get allowed html tags
  $allowed_html = get_allowed_html();

  return filter_xss($string, $allowed_html);
  
}