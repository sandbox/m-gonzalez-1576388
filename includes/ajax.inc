<?php
/**
 * @file ajax.inc
 * @author Martin Gonzalez
 */


/**
 * detect an AJAX request 
 * This is a simple function, that can be used in an ajax callback function 
 * to determinate if is an ajax process or is a normal page request directly in the browser
 * @return TRUE if AJAX request, FALSE if is a regular page
 */
function check_ajax_request() {
  
  if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    return TRUE;
  }
  
  return FALSE;
}